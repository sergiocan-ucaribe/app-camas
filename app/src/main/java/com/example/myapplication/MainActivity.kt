package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.example.myapplication.Patient
import com.example.myapplication.Bed
import com.example.myapplication.Hospital

import com.example.myapplication.Utils

class MainActivity : AppCompatActivity() {
    private var _myHospitals = MutableList(3) { Hospital() }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // Write a message to the database
        val database = FirebaseDatabase.getInstance()
        val myRef = database.getReference("Hospitales")
        this._myHospitals = createHospitals()
        myRef.setValue(this._myHospitals)
        AssignBeds();
        myRef.setValue(this._myHospitals)
    }

    fun AssignBeds(){
        for(hospital in this._myHospitals){
            var newHospital : Hospital

            if (hospital.sortType == "llegada"){
                newHospital =  arrivalPriority(hospital)

            } else {
                newHospital =  emergencyPriority(hospital)
            }
            hospital.bed_terapy = newHospital.bed_terapy;
            hospital.bed = newHospital.bed;
        }
    }
    fun emergencyPriority(hospital: Hospital ): Hospital {
        val patients = hospital.patients ?: listOf();
        // Patients are shuffled so order by Score
        // 0 needs equipment to 1 is healty
        // if is hospital staff will get priority as long as is in bad state
        val sortedPatients = patients?.sortedWith(compareBy ({it.averageScore},{it.isHospitalStaff}));
        var normalBedIteration = 0
        var respiratorBedIteration = 0

        for(patient in sortedPatients){
            if (patient.needRespirator == true && respiratorBedIteration >= hospital.bed_terapy?.size!!){
                print("Error no hay camas con respirador disponibles")
                continue
            }
            if (patient.needRespirator == false && normalBedIteration >= hospital.bed?.size!!){
                print("Error no hay camas normales disponibles")
                continue
            }
            if (patient.needRespirator == false){
                hospital.bed?.get(normalBedIteration)?.patient = patient;
                normalBedIteration+=1
            } else {
                hospital.bed_terapy?.get(respiratorBedIteration)?.patient = patient;
                respiratorBedIteration+=1
            }
        }
        return hospital;
    }

    fun arrivalPriority(hospital: Hospital ): Hospital{
        val patients = hospital.patients ?: listOf();
        // Patients are shuffled so order by arrival
        val sortedPatients = patients?.sortedWith(compareBy({it.arrivalOrder}));
        var normalBedIteration = 0
        var respiratorBedIteration = 0

        for(patient in sortedPatients){
            if (patient.needRespirator == true && respiratorBedIteration >= hospital.bed_terapy?.size!!){
                print("Error no hay camas con respirador disponibles")
                continue
            }
            if (patient.needRespirator == false && normalBedIteration >= hospital.bed?.size!!){
                print("Error no hay camas normales disponibles")
                continue
            }
            if (patient.needRespirator == false){
                hospital.bed?.get(normalBedIteration)?.patient = patient;
                normalBedIteration+=1
            } else {
                hospital.bed_terapy?.get(respiratorBedIteration)?.patient = patient;
                respiratorBedIteration+=1
            }
        }
        return hospital;
    }

    fun createPatients() : MutableList<Patient> {
        var Patients = MutableList<Patient>(10) { Patient() };
        for (patientCount in 0..10){
            Patients[patientCount] = Patient(
                Utils.GenerateName(),
                Utils.getAvgScore(),
                Utils.getRandomBoolean(),
                Utils.getRandomBoolean(),
                patientCount
            );
        }

        return Patients.shuffled().toMutableList();
    }

    fun createBeds(withRespirator: Boolean ?=false) : MutableList<Bed> {
        var Beds = MutableList<Bed>(6) {  Bed() };
        for (bedCount in 0..6){
            Beds[bedCount] = Bed(
                withRespirator
            );
        }
        return Beds;
    }

    fun createHospitals() : MutableList<Hospital> {
        val Hospitals = MutableList<Hospital>(3){ Hospital() }
        for (hospitalCount in 0..3){
            Hospitals[hospitalCount] = Hospital (
                Utils.GenerateHospitalName(),
                createBeds(true),
                createBeds(),
                createPatients(),
                Utils.getHospitalStatus()
            )
        }
        return Hospitals;
    }
}
