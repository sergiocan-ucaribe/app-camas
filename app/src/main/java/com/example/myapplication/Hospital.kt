package com.example.myapplication
import com.example.myapplication.Bed;
import com.example.myapplication.Patient;

data class Hospital(
    var name: String? = "",
    var bed_terapy: MutableList<Bed>? = null,
    var bed: MutableList<Bed>? = null,
    var patients: MutableList<Patient>? = null,
    var sortType: String?="llegada"
)