package com.example.myapplication

data class Patient(
    var name: String? = "",
    var averageScore: Double ? = 0.0,
    var isHospitalStaff: Boolean? = false,
    var needRespirator: Boolean ? = false,
    var arrivalOrder: Int ? = 0
)