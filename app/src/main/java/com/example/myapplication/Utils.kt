package com.example.myapplication
import kotlin.random.Random.Default.nextInt
import kotlin.random.Random.Default.nextDouble
import kotlin.random.Random.Default.nextBoolean

object Utils {
     fun GenerateName(): String {
        val names = listOf(
            "Sergio",
            "Jose",
            "Luis",
            "Maria",
            "Fernando",
            "Ricardo"
        )
        var lastNames = listOf(
            "Can",
            "Lopez",
            "Prado",
            "Mendez",
            "Alejo",
            "Martinez"
        )
        val builder = StringBuilder()
        builder.append(names.indexOf(nextInt(6)))
            .append(" ")
            .append(lastNames.indexOf(nextInt(6)))
        return builder.toString();
    }
    fun GenerateHospitalName(): String {
        val names = listOf(
            "TeKuro",
            "Paso al",
            "Viendo"
        )
        var lastNames = listOf(
            "otemato",
            "cielo",
            "las estrellas"
        )
        val builder = StringBuilder()
        builder.append(names.elementAt(nextInt(3)))
            .append(" ")
            .append(lastNames.elementAt(nextInt(3)))
        return builder.toString();
    }
    fun getAvgScore(): Double {
        return nextDouble(0.0,1.0);
    }
    fun getRandomBoolean(): Boolean {
        return nextBoolean()
    }
    fun getHospitalStatus(): String {
        val statuses = listOf(
            "llegada",
            "emergencia"
        )
        return statuses.elementAt(nextInt(2));
    }
}