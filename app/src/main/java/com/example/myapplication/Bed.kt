package com.example.myapplication

data class Bed(
    var hasRespirator: Boolean?=false,
    var patient: Patient?=null
)